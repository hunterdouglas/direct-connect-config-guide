# Configuring Provisioning for Direct Connect

This guide provides the steps required to configure SCIM Provisioning for Direct Connect within Okta.

## Features

Direct Connect supports the following SCIM features:
    
  * Create Users
  * Update User Attributes
  * Deactivate Users

## Requirements

You will need to know the Direct Connect site for which you're configuring Provisioning (e.g. production US, QA Canada, etc.).

You will also need an API token for the Direct Site for which you're configuring Provisioning. Contact [Doug Douglass <doug.douglass@hunterdouglas.com>](mailto:doug.douglass@hunterdouglas.com), [Ruben Rodriguez <ruben.rodriguez@picbusiness.com\>](mailto:ruben.rodriguez@picbusiness.com) or [Frank Louvado <frank.louvado@hunterdouglas.com\>](mailto:frank.louvado@hunterdouglas.com) to acquire an API token.


## Configuration Instructions

1. As an Okta Administrator, select  **Applications**, **Add Application** and enter "Direct Connect" into the search box. If more than one application is found, select the one that is *Okta Verified*.
   
    ![alt text](app-search.png)

1. On the *General Settings* tab

    ![alt text](general.png)
    
    1. enter an appropriate label
    1. Optionally, check the *Do not display application icon in the Okta Mobile App* as mobile users should be using the Hunter Douglas *Mobile Selling App* (iOS) to access Direct Connect.
    1. Click **Done**
    
1. On the *Provisioning* tab, click **Configure API Integration**

    ![alt text](provisioning-1.png)

1. Check the **Enable API Integration** box

    ![alt text](provisioning-2.png)

    1. Enter the SCIM API **Base URL** (e.g. https://dc.picbusiness.com/api/index.php/scim/v2)
    1. Enter the **API Token**
    
1. Optionally, click **Test API Credentials**

    ![alt text](provisioning-3.png)

1. Click **Save**

1. On the *Provisioning* tab, click **To App**

    ![alt text](provisioning-4.png)
    
    1. Check the **Enable** boxes for **Create Users**, **Update User Attributes** and **Deactivate Users**
    1. Click **Save**

1. You can now assign people or groups to the app.

## Notes

* There should be no need to adjust **Attribute Mappings**
* There should be no need to either adjust settings of nor perform an import from Direct Connect to Okta

